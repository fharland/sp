/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Modules -> Blank
*/

/* ----------------------------------------------- */
/**
*	Blank
*/

	App.Blank = (function() {

        var obj = {};

        /* --------------------------------------- */

		// 	Elements
		var $win,
			$doc,
			$html,
			$body,
			$page;


        /* --------------------------------------- */
        /**
        *   Blank -> init()
        */

		obj.init = function() {
			$win 	= $(window);
			$doc 	= $(document);
			$html 	= $('html');
			$body 	= $('body') || $(document.body);
			$page 	= $('html, body');

            addEvents();
		};


        /* --------------------------------------- */
        /**
        *   Blank -> addEvents()
        */

		function addEvents() {

		}


        /* --------------------------------------- */

        return obj;


    })();


/* ----------------------------------------------- */
