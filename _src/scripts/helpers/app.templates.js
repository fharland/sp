/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Helpers -> Templates
*/

/* ----------------------------------------------- */
/**
*   Templates
*/

    App.Templates = (function() {

        var obj = {};

        /* --------------------------------------- */

        //  Elements
        var $html;


        /* --------------------------------------- */
        /**
        *   Templates -> init()
        */

        obj.init = function() {
        };


        /* --------------------------------------- */
        /**
        *   Templates -> getBlankTemplate()
        */

        obj.getBlankTemplate = function() {
            $html = $('<div/>', {'class': 'blank'});

            return $html;
        };


        /* --------------------------------------- */

        return obj;


    })();


/* ----------------------------------------------- */
