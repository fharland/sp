$(document).ready(function(){

	/* ------------------------------------------- */
	/**
	*	Global Variables & caching $elements
	*/

		// -> Properties
		debug			= false;


		// -> Objects
		time 			= 300;
		swift 			= 'cubic-bezier(0.55, 0, 0.1, 1)';
		back 			= 'cubic-bezier(0.180, 0.020, 0.210, 1.500)';


		// -> Elements
		$win			= $(window);
		$doc			= $(document);

		$html			= $('html');
		$body 			= $('body');
		$page 			= $('html, body');


		// -> Clickables


		// -> Dynamics


		// -> Breakpoints
		breakpoint_xs 	= 480;
		breakpoint_sm 	= 768;
		breakpoint_md 	= 992;
		breakpoint_lg 	= 1200;
		breakpoint_xl 	= 1828;


	/* ------------------------------------------- */
	/**
	*	App -> Window Load
	*/

		$win.load( function(event) {
			if (typeof App.preCache === 'function') {
				App.preCache();
			}
		});


	/* ------------------------------------------- */
	/**
	*	App -> Window Scroll
	*/

		$win.scroll( App.debounce( function(event) {

		}, 150 ));


	/* ------------------------------------------- */
	/**
	*	App -> Window Resize
	*/

		$win.resize( App.debounce( function(event) {

		}, 150 ));


	/* ------------------------------------------- */
	/**
	*	App -> Behaviour
	*/

		//	App init
		App.init();


		//	set debugMode 
		App.isDebugMode();


	/* ------------------------------------------- */

});