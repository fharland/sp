/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities -> Throttle
*/

/* ----------------------------------------------- */
/**
*	Utility -> App.throttle()
*	[description]
*
*/

	App.throttle = function( func, wait, options ) {
	
		var args,
			context,
			later,
			now,
			previous,
			remaining,
			result,
			timeout;
	
		timeout 	= null;
		previous 	= 0;
		options 	= !options ? {} : options;
	
		later = function() {
	
		  previous 	= options.leading === false ? 0 : new Date().getTime();
		  result 	= func.apply( context, args );
		  timeout 	= null;
	
		  if (!timeout) {
			  context = args = null;
		  }
	
		};
	
		return function() {
	
		  now = new Date().getTime();
	
		  if ( !previous && !options.leading ) {
			previous = now;
		  }
	
		  args 		= arguments;
		  context 	= this;
		  remaining = wait - ( now - previous );
	
		  if ( remaining <= 0 || remaining > wait ) {
	
			if ( timeout ) {
			  clearTimeout( timeout );
			  timeout = null;
			}
	
			previous 	= now;
			result 		= func.apply( context, args  );
	
			if ( !timeout ) {
				context = args = null;
			}
	
		  } else if ( !timeout && !options.trailing ) {
			timeout = setTimeout( later, remaining );
		  }
	
		  return result;
	
		};
	
	};

/* ----------------------------------------------- */
