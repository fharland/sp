/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities
*/

/* ----------------------------------------------- */
/**
*	Index
*
*	[1]  App.getNow()
*	[2]  App.getRandom( min, max )
*	[3]  App.getScrollPosition()
*	[4]  App.getTypeOf(obj)
*
*	[5]	 App.isDebugMode()
*	[6]  App.isEqual( a, b )
*	[10] App.isValidEmail(obj)
*
*	[11] App.svgFallback()
*	[12] App.windowSize() 
*	[13] App.hexSha(obj)
*	[14] App.capitalize(obj)
*
*	[15] Localization | Extends Date prototype
*	[16] XHR Progress | Extends XHR/Ajax prototype
*
*/

/* ----------------------------------------------- */
/**
*	Utility -> new Date().getWeek()
*	returns the current weeknumber
*
*	@return	Timestamp
*/

	Date.prototype.getWeek = function() {
		var onejan 		= new Date(this.getFullYear(), 0, 1),
			today 		= new Date(this.getFullYear(), this.getMonth(), this.getDate()),
			dayOfYear 	= ((today - onejan + 86400000) / 86400000);

		return Math.ceil(dayOfYear / 7);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getNow()
*	returns the current timestamp
*
*	@return	Timestamp
*/

	App.getNow = Date.now || function() {
		return new Date().getTime();
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getRandom()
*	returns random number between min & max
*
*	@param	Int	min
*	@param	Int	max
*
*	@return	Int
*/

	App.getRandom = function( min, max ) {
		max = (App.isNull(max) || App.isUndefined(max)) ? min : max;
		min = (App.isNull(max) || App.isUndefined(max)) ? 0 : min;

		return min + Math.floor(Math.random() * ((max - min) +1));
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getScrollPosition()
*	returns the current scroll-position
*
*	@return	Array	{ top: y, left: x }
*/

	App.getScrollPosition = function() {
		var a 	= document.body,
			b 	= document.documentElement,
			c	= { top: 0, left: 0 };

		if ( a && ( a.scrollLeft || a.scrollTop ) ) {

			c.top 	= a.scrollTop;
			c.left = a.scrollLeft;

		} else if ( b && ( b.scrollLeft || b.scrollTop ) ) {

			// IE6

			c.top 	= b.scrollTop;
			c.left = b.scrollLeft;

		}

		return c;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getTypeOf()
*	returns the true type of an object
*
*	@param	Mixed	obj
*
*	@return	Boolean
*/

	App.getTypeOf = function(obj) {
		var a = typeof obj;

		if ( a === 'object' ) {
			if (obj instanceof Array) {
				a = 'array';
			}

			if (App.isNull(obj)) {
				a = 'null';
			}
		}

		return a;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isDebugMode()
*	logs if Debug Mode is on or not
*
*	@return	Boolean
*/

	App.isDebugMode = function() {
		if (App.debugMode) {
			console.info('Debug Mode', true);
		}

		return App.debugMode;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isEqual()
*	returns a deep comparison to check if two values are equal
*
*	@param	Mixed	a
*	@param	Mixed	b
*
*	@return	Boolean
*/

	App.isEqual = function(a, b) {
		return eq(a, b);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isValidEmail()
*	returns wether the provided string is a valid e-mailaddress
*
*	@return	Boolean
*/

	App.isValidEmail = function(obj) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

		return pattern.test(obj);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isValidPhonenumber()
*	returns wether the provided string is a valid phonenumber
*
*	@return	Boolean
*/

	App.isValidPhonenumber= function(obj) {
		var pattern = new RegExp(/^[\d\.\-]+$/);

		return pattern.test(obj);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.svgFallback()
*	replaces '.svg' with '.png' if svg is not supported
*
*	@requires Modernizr (vendor)
*/

	App.svgFallback = function() {

		var svgs 	= $('img[src$=".svg"]'),
			svg 	= null;

		if (!Modernizr.svg) {
			svgs.each( function() {
				svg = $(this);
				svg.attr('src', svg.attr('src').replace( '.svg', '.png' ) );
			});
		}
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.windowSize()
*	returns the current window size
*
*	@return	Array	{ width: a, height: b }
*/

	App.windowSize = function() {
		var win = $(window);
		
		return {
			width: win.outerWidth(),
			height: win.outerHeight()
		};
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.hexSha()
*	returns a hashed result of a provided variable
*
*	@return	String
*
*	@requires sha512.js (vendor)
*/

	App.hexSha = function(obj) {
		return hex_sha512(obj);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.disableAutoComplete()
*	disables auto-complete for provided collection of elements
*
*	@param	mixed	collection
*/

	App.disableAutoComplete = function(collection) {
		collection.attr({ 'autocomplete': 'off' });
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.capitalize()
*	capitalizes the provided string
*	(uppercase first character)
*
*	@param	String 	
*
*	@return	String 	
*/

	App.capitalize = function(string, separator) {
		separator = typeof separator === 'undefined' ? ' ' : separator;

		var arr     	= string.split(separator),
		    tempString 	= '',
		    newString  	= [];

		for (var i = 0; i < arr.length; i++) {
		    tempString = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
		    newString.push(tempString);
		}

		return newString.join(separator);
	};


/* ----------------------------------------------- */
/**
*	Utility -> Localization (Date)
* 	Default language: EN
* 	(optional parameter)
*
*	@param 	{string} 	lang
*
*	eg.
*	new Date().getDayName()
*
*/

	Date.prototype.getDayName = function(lang) {
	    lang = lang && (lang in Date.locale) ? lang : 'en';
	    return Date.locale[lang].days[this.getDate()];
	};

	Date.prototype.getDayNameShort = function(lang) {
	    lang = lang && (lang in Date.locale) ? lang : 'en';
	    return Date.locale[lang].days_short[this.getDate()];
	};

	Date.prototype.getMonthName = function(lang) {
	    lang = lang && (lang in Date.locale) ? lang : 'en';
	    return Date.locale[lang].months_names[this.getMonth()];
	};

	Date.prototype.getMonthNameShort = function(lang) {
	    lang = lang && (lang in Date.locale) ? lang : 'en';
	    return Date.locale[lang].months_short[this.getMonth()];
	};

	Date.locale = {
		en: {
			months: 		['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			months_short: 	['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			days: 			['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			days_short: 	['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
	    },
		nl: {
			months: 		['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
			months_short: 	['jan', 'feb', 'maa', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
			days: 			['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
			days_short: 	['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za']
	    }
	};


/* ----------------------------------------------- */
/**
*	Utility -> Enable progress tracking for XHR calls
*	
*	Adds a progress listener to AJAX-calls
*	eg.
*	$.ajax({
*		url: '...',
*		data: '...',
*		success: function() {
*			(...do on success...)
*		},
*		error: function() {
*			(...do on error...)
*		},
*		progress: function() {
*			(...do on progress...)
*		},
*	});
*/

	(function($, window, undefined) {
	    // Is onprogress supported by browser?
	    var hasOnProgress = ('onprogress' in $.ajaxSettings.xhr());

	    // If not supported, do nothing
	    if (!hasOnProgress) {
	        return;
	    }
	    
	    // Patch ajax settings to call a progress callback
	    var oldXHR = $.ajaxSettings.xhr;

	    $.ajaxSettings.xhr = function() {
	        var xhr = oldXHR();

	        if(xhr instanceof window.XMLHttpRequest) {
	            xhr.addEventListener('progress', this.progress, false);
	        }
	        
	        if(xhr.upload) {
	            xhr.upload.addEventListener('progress', this.progress, false);
	        }
	        
	        return xhr;
	    };

	})(jQuery, window);


/* ----------------------------------------------- */

