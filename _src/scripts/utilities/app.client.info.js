/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities -> Client Information
*/

/* ----------------------------------------------- */
/**
*	Index
*
*	[1]  App.cookiesEnabled()
*
*	[2]  App.getBrowser()
*	[3]  App.getBrowserVersion()
*	[4]  App.getOS()
*	[5]  App.getOSVersion()
*	[6]  App.getScreenSize()
*
*	[7]	 App.onDevice()
*	[8]  App.onMobile()
*	[9]  App.onTablet()
*
*	[10] App.getClientInfo()
*
*/

/* ----------------------------------------------- */
/**
*	Utility -> App.cookiesEnabled()
*	returns wether cookies are enabled or not
*
*	@return	Boolean
*/

	App.cookiesEnabled = function() {
		return App.getClientInfo().cookies;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getBrowser()
*	returns the current browser
*
*	@return	String
*/

	App.getBrowser = function() {
		return App.getClientInfo().browser;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getBrowserVersion()
*	returns the current browser-version
*
*	@return	String
*/

	App.getBrowserVersion = function() {
		return App.getClientInfo().browserVersion;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.onDevice()
*	returns wether a mobile device is used or not
*
*	@return	Boolean
*/

	App.onDevice = function() {
		return App.onMobile() && App.onTablet();
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.onMobile()
*	returns wether a mobile phone is used or not
*
*	@return	Boolean
*/

	App.onMobile = function() {
		return App.getClientInfo().mobile;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.onTablet()
*	returns wether a tablet device is used or not
*
*	@return	Boolean
*/

	App.onTablet = function() {
		return App.getClientInfo().tablet;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getOS()
*	returns the current OS
*
*	@return	String
*/

	App.getOS = function() {
		return App.getClientInfo().os;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getOSVersion()
*	returns the current OS-version
*
*	@return	String
*/

	App.getOSVersion = function() {
		return App.getClientInfo().osVersion;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getScreenSize()
*	returns the current screen-size
*
*	Note: does not return the current window size
*
*	@return	String
*/

	App.getScreenSize = function() {
		return App.getClientInfo().screen;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.getClientInfo()
*	returns an array containing client information
*
*	@return	Array
*	{
		browser:			String		'current browser'
		browserVersion:		String		'current browser-version'
		cookies: 			Boolean		'cookies enabled'
		mobile:				Boolean		'mobile phone'
		os:					String		'current OS'
		osVersion:			String		'current OS-version'
		tablet:				Boolean		'tablet device'
		screen:				String		'current screen-size'
	}
*/

	App.getClientInfo = function() {

		var unknown = 'unknown';

		// screen
		var screenSize 	= '',
			screen		= window.screen;

		if (screen.width) {
			width 	= (screen.width) ? screen.width : '';
			height 	= (screen.height) ? screen.height : '';

			screenSize += '' + width + ' x ' + height;
		}

		//	browser
		var nVer 			= navigator.appVersion,
			nAgt 			= navigator.userAgent,
			browser 		= navigator.appName,
			version 		= '' + parseFloat( navigator.appVersion ),
			majorVersion 	= parseInt( navigator.appVersion, 10 ),
			nameOffset, verOffset, ix;

		// Opera
		if ( ( verOffset = nAgt.indexOf('Opera') ) !== -1 ) {
			browser = 'Opera';
			version = nAgt.substring(verOffset + 6);
			if ((verOffset = nAgt.indexOf('Version')) !== -1) {
				version = nAgt.substring(verOffset + 8);
			}
		}

		// MSIE (Microsoft Internet Explorer 10-)
		else if ((verOffset = nAgt.indexOf('MSIE')) !== -1) {
			browser = 'Microsoft Internet Explorer';
			version = nAgt.substring(verOffset + 5);
		}

		// Chrome
		else if ((verOffset = nAgt.indexOf('Chrome')) !== -1) {
			browser = 'Chrome';
			version = nAgt.substring(verOffset + 7);
		}

		// Safari
		else if ((verOffset = nAgt.indexOf('Safari')) !== -1) {
			browser = 'Safari';
			version = nAgt.substring(verOffset + 7);
			if ((verOffset = nAgt.indexOf('Version')) !== -1) {
				version = nAgt.substring(verOffset + 8);
			}
		}

		// Firefox
		else if ((verOffset = nAgt.indexOf('Firefox')) !== -1) {
			browser = 'Firefox';
			version = nAgt.substring(verOffset + 8);
		}

		// MSIE 11+ (Microsoft Internet Explorer 11+)
		else if (nAgt.indexOf('Trident/') !== -1) {
			browser = 'Microsoft Internet Explorer';
			version = nAgt.substring(nAgt.indexOf('rv:') + 3);
		}

		// Other browsers
		else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
			browser = nAgt.substring(nameOffset, verOffset);
			version = nAgt.substring(verOffset + 1);
			if (browser.toLowerCase() === browser.toUpperCase()) {
				browser = navigator.appName;
			}
		}

		// trim the version string
		if ((ix = version.indexOf(';')) !== -1) version = version.substring(0, ix);
		if ((ix = version.indexOf(' ')) !== -1) version = version.substring(0, ix);
		if ((ix = version.indexOf(')')) !== -1) version = version.substring(0, ix);

		majorVersion = parseInt('' + version, 10);
		if (isNaN(majorVersion)) {
			version = '' + parseFloat(navigator.appVersion);
			majorVersion = parseInt(navigator.appVersion, 10);
		}

		// mobile phone
		var mobile = /Mobile|mini|Fennec|Android|iP(hone|od)/.test(nVer);

		// tablet device
		var tablet = /iPad/.test(nVer);

		// cookies
		var cookieEnabled = (navigator.cookieEnabled) ? true : false;

		if (typeof navigator.cookieEnabled === 'undefined' && !cookieEnabled) {
			document.cookie = 'testcookie';
			cookieEnabled = (document.cookie.indexOf('testcookie') !== -1) ? true : false;
		}

		// OS (Operating System)
		var os = unknown;
		var clientStrings = [
			{s:'Windows 3.11', r:/Win16/},
			{s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
			{s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
			{s:'Windows 98', r:/(Windows 98|Win98)/},
			{s:'Windows CE', r:/Windows CE/},
			{s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
			{s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
			{s:'Windows Server 2003', r:/Windows NT 5.2/},
			{s:'Windows Vista', r:/Windows NT 6.0/},
			{s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
			{s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
			{s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
			{s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
			{s:'Windows ME', r:/Windows ME/},
			{s:'Android', r:/Android/},
			{s:'Open BSD', r:/OpenBSD/},
			{s:'Sun OS', r:/SunOS/},
			{s:'Linux', r:/(Linux|X11)/},
			{s:'iOS', r:/(iPhone|iPad|iPod)/},
			{s:'Mac OS X', r:/Mac OS X/},
			{s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
			{s:'QNX', r:/QNX/},
			{s:'UNIX', r:/UNIX/},
			{s:'BeOS', r:/BeOS/},
			{s:'OS/2', r:/OS\/2/},
			{s:'Search Bot', r:/(nuhk|Googlebot|Bing Bot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
		];
	
		for (var id in clientStrings) {
			var cs = clientStrings[id];
			if (cs.r.test(nAgt)) {
				os = cs.s;
				break;
			}
		}
	
		// OS-version (Operating System Version)
		var osVersion = unknown;
	
		if (/Windows/.test(os)) {
			osVersion = /Windows (.*)/.exec(os)[1];
			os = 'Windows';
		}
	
		switch (os) {
			case 'Mac OS X':
				osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
				break;
	
			case 'Android':
				osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
				break;
	
			case 'iOS':
				osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
				osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
				break;
		}

		return {
			browser: browser,
			browserVersion: version,
			cookies: cookieEnabled,
			mobile: mobile,
			os: os,
			osVersion: osVersion,
			screen: screenSize,
			tablet: tablet
		};
	
	};


/* ----------------------------------------------- */
