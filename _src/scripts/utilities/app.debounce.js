/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities -> Debounce
*/

/* ----------------------------------------------- */
/**
*	Utility -> App.debounce()
*	[description]
*
*/

	App.debounce = function( func, wait, immediate ) {
	
		var args,
			call_now,
			context,
			last,
			later,
			result,
			timeout,
			timestamp;
	
		later = function() {
	
		  last = new Date().getTime() - timestamp;
	
		  if ( last < wait && last >= 0 ) {
			timeout = setTimeout( later, wait - last );
	
		  } else {
			timeout = null;
	
			if ( !immediate ) {
			  result = func.apply( context, args );
	
			  if ( !timeout ) {
				  context = args = null;
			  }
	
			}
	
		  }
	
		};
	
		return function() {
	
		  args 		= arguments;
		  callNow 	= immediate && !timeout;
		  context 	= this;
		  timestamp = new Date().getTime();
	
		  if ( !timeout ) {
			  timeout = setTimeout( later, wait );
		  }
	
		  if ( call_now ) {
			result 	= func.apply( context, args );
			context = args = null;
		  }
	
		  return result;
	
		};
	
	};

/* ----------------------------------------------- */
