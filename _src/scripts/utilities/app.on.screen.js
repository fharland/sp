/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities -> IsOnScreen
*/

/* ----------------------------------------------- */
/**
*	Utility -> App.isOnScreen()
*	returns true if Element (x,y) is in the current viewport
*
*	@return	Boolean
*/

	App.isOnScreen = function( el, x, y ) {
	
		/*
			el: element to check
			x: 	point of element to check ( x-coordinate )
			y: 	point of element to check ( y-coordinate )
				(eg. 0.1 would be top-left, 1.0 would be bottom-right)
				(default is 0.1)
		*/
	
		var win 		= $(window),
			bounds 		= {},
			deltas 		= {},
			viewport 	= {},
			visible 	= false,
			height 		= 0,
			width 		= 0,
			elX 		= 0.1,
			elY 		= 0.1;
	
		elX = ( x === null || typeof x === 'undefined' ) ? 0.1 : x;
		elY = ( y === null || typeof y === 'undefined' ) ? 0.1 : y;
		
		viewport = {
			top 	: win.scrollTop(),
			right 	: win.scrollLeft() + win.width(),
			bottom 	: win.scrollTop() + win.height(),
			left 	: win.scrollLeft()
		};
	
		height 	= el.outerHeight();
		width 	= el.outerWidth();
		
		if( !width || !height ){
			return false;
		} else {
	
			bounds 			= el.offset();
			bounds.right 	= bounds.left + width;
			bounds.bottom 	= bounds.top + height;
	
			visible = ( !( viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom ));
	
			if ( !visible ) {
				return false;
			}
	
			deltas = {
				top: 	Math.min( 1, ( bounds.bottom - viewport.top ) /height ),
				right: 	Math.min( 1, ( viewport.right - bounds.left ) /width ),
				bottom: Math.min( 1, ( viewport.bottom - bounds.top ) /height ),
				left: 	Math.min( 1, ( bounds.right - viewport.left ) /width )
			};
	
			return ( deltas.left * deltas.right ) >= elX && ( deltas.top * deltas.bottom ) >= elY;
	
		}
		
	};

/* ----------------------------------------------- */
