/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Utilities -> Check Types
*/

/* ----------------------------------------------- */
/**
*	Index
*
*	[1]  App.isArray(data)
*	[2]  App.isBoolean(data)
*	[3]  App.isDate(data)
*	[4]  App.isEven(data)
*	[5]  App.isFunction(data)
*	[6]	 App.isImage(data)
*	[7]	 App.isInteger(data)
*	[8]  App.isNull(data)
*	[9]  App.isNumber(data)
*	[10] App.isObject(data)
*	[11] App.isOdd(data)
*	[12] App.isString(data)
*	[13] App.isStringNonEmpty(data)
*	[14] App.isUndefined(data)
*	[15] App.isWebUrl(data)
*	[16] App.isSet(data)
*
*	[17] App.areEqual(a, b)
*
*/

/* ----------------------------------------------- */
/**
*	Utility -> App.isArray()
*	returns true if data is an Array
*
*	@return	{boolean}
*/

	App.isArray = function(data) {
		return Array.isArray(data);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isBoolean()
*	returns true if data is a Boolean
*
*	@return	{boolean}
*/

	App.isBoolean = function(data) {
		return data === false || data === true;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isDate()
*	returns true if data is a valid Date
*
*	@return	{boolean}
*/

	App.isDate = function(data) {
		return 	Object.prototype.toString.call(data) === '[object Date]' &&
				!isNaN(data.getTime());
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isEven()
*	returns true if data is Even
*
*	@return	{boolean}
*/

	App.isEven = function(data) {
		return number(data) && data %2 === 0;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isFunction()
*	returns true if data is a Function
*
*	@return	{boolean}
*/

	App.isFunction = function(data) {
		return typeof data === 'function';
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isImage()
*	returns true if data is an image
*
*	@return	{boolean}
*/

    App.isImage = function(data) {
      return data.match(/^data:image\/(bmp|gif|jpg|jpeg|png|svg)/gi);
    };


/* ----------------------------------------------- */
/**
*	Utility -> App.isInteger()
*	returns true if data is an integer
*
*	@return	{boolean}
*/

	App.isInteger = function(data) {
		return number(data) && data %1 === 0;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isNull()
*	returns true if data is Null
*
*	@return	{boolean}
*/

	App.isNull = function(data) {
		return data === null;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isNumber()
*	returns true if data is a Number
*
*	@return	{boolean}
*/

	App.isNumber = function(data) {
		return 	typeof data === 'number' && isNaN(data) === false &&
				data !== Number.POSITIVE_INFINITY &&
				data !== Number.NEGATIVE_INFINITY;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isObject()
*	returns true if data is an Object
*
*	@return	{boolean}
*/

	App.isObject = function(data) {
		return App.isSet(data) && Object.protoype.toString().call(data) === '[object Object]';
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isOdd()
*	returns true if data is Odd
*
*	@return	{boolean}
*/

	App.isOdd = function(data) {
		return number(data) && !App.isEven(data);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isPrimitive()
*	returns true if data is a Primitive value
*	(undefined, null, number, boolean, string)
*
*	@return	{boolean}
*/

	App.isPrimitive = function(data) {  
		return (App.isUndefined(data) ||
				App.isNull(data) ||
				App.isNumber(data) ||
				App.isBoolean(data) ||
				App.isString(data));
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isString()
*	returns true if data is a String
*
*	@return	{boolean}
*/

	App.isString = function(data) {
		return typeof data === 'string';
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isStringNonEmpty()
*	returns true if data is not an empty String
*
*	@return	{boolean}
*/

	App.isStringNonEmpty = function(data) {
		return App.isString(data) && ($.trim(data).length > 0);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isWebUrl()
*	returns true if data is a valid Web-Url (http & https)
*
*	@return	{boolean}
*/

	App.isWebUrl = function(data) {
		return App.isUnemptyString(data) && /^(https?:)?\/\/([\w-\.~:@]+)(\/[\w-\.~\/\?#\[\]&\(\)\*\+,;=%]*)?$/.test(data);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isUndefined()
*	returns true if data Undefined
*
*	@return	{boolean}
*/

	App.isUndefined = function(data) {
		return (data === undefined || typeof data === 'undefined') && data === void 0;
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.isSet()
*	returns true if data is Assigned
*
*	@return	{boolean}
*/

	App.isSet = function(data) {
		return !App.isUndefined(data) && !App.isNull(data);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.areEqual()
*	returns true if a and b are equal
*
*	@return	{boolean}
*/

	App.areEqual = function(a, b) {
		return (a.length === b.length) && (a === b);
	};


/* ----------------------------------------------- */
/**
*	Utility -> App.areEqualDeep()
*	returns true if a and b are equal
*
*	@param 	{object|mixed} 	a
*	@param 	{object|mixed} 	b
*
*	@return	{boolean}
*/

	App.areEqualDeep = function (a, b) {

		if ( App.isPrimitive(a) && App.isPrimitive(b) ) {
			return a === b;
		} else if ( App.isPrimitive(a) || App.isPrimitive(b) ) {
			return false;
		} else {

			var aKeys = Object.keys(a);
			var bKeys = Object.keys(b);

			if (aKeys.length !== bKeys.length) {
				return false;
			} else {

				for ( var prop in a ) {
					if (App.isUndefined(b[prop])) {
						return false;
					} else if (!App.areEqual(a[prop], b[prop])) {
						return false;
					}
				}

				return true;
			}
		}
	};


/* ----------------------------------------------- */
