/**
*   Concrete 5 Default
*   JS
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ App -> FX
*/

/* ----------------------------------------------- */
/**
*	App -> init()
*
*/

	var App = {
	
		init: function() {

			var $startScreen = $('#start-screen');

			// Init FastClick
			FastClick.attach(document.body);

			// Add Global Event Handlers
			this.addEvents();

			// Launch templates based on content
			if ($startScreen.length > 0) {
				App.StartScreen.init();
			}
		},
	
	
		addEvents: function() {
			console.log('Global events added');
		}
	};







/* ----------------------------------------------- */
