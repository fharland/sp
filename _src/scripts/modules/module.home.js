/* ----------------------------------------------- */
/**
*	Startscreen
*/

	App.StartScreen = (function() {

        var obj = {};

        /* --------------------------------------- */

		// 	Elements
		var $win,
			$doc,
			$html,
			$body,
			$page;

        /* --------------------------------------- */
        /**
        *   Blank -> init()
        */

		obj.init = function() {
			$win 	= $(window);
			$doc 	= $(document);
			$html 	= $('html');
			$body 	= $('body') || $(document.body);
			$page 	= $('html, body');

            addEvents();
        	addBaseEffects();

		};

		obj.sounds = {
			klaas  : new Audio('dist/assets/sounds/test.m4a'),
			alarm1 : new Audio('dist/assets/sounds/alarm1.mp3'),
			alarm2 : new Audio('dist/assets/sounds/alarm2.mp3')
		};


        /* --------------------------------------- */
        /**
        *   Blank -> addEvents()
        */

		function addEvents() {
			console.log('hi');
		}

		/* --------------------------------------- */
        /**
        *   Blank -> addBaseEffects()
        */

		function addBaseEffects() {

			var $startScreen  = $('#start-screen');
			var $moon         = $('#moon');
			var $letter       = $('#letter');
			var letterStrings = [
							'Lieve snooky',
							'Hey ouwe prutser,',
							'Beste <b>Jeroen</b>, <br><br> Hoor de wind waait door de bomen! <br> Het heerlijk avondje is weer gekomen. <br><br> Sint &amp; Piet zaten ook dit jaar weer te denken, <br> Wat ze die knul nu weer moesten '
						  ];
			var $overlay = $('#overlay');
			var $jail    = $('#jail');
			var $terminal = $('#terminal');


			$startScreen.addClass('animated');

			addSounds();
			
			setTimeout(function(){
				$moon.addClass('animated');
			}, 750);

			setTimeout(function(){
				$letter.addClass('animated').find('.inner').typed({ 
					strings: letterStrings,
					typeSpeed: 25,
					startDelay: 2000,
					showCursor: true,
					backDelay: 200,
		            cursorChar: "|",
		            callback: function() {
		            	App.StartScreen.sounds.klaas.pause();

		            	App.StartScreen.sounds.alarm1.addEventListener('ended', function() {
						    this.currentTime = 0;
						    this.play();
						}, false);

						App.StartScreen.sounds.alarm2.addEventListener('ended', function() {
						    this.currentTime = 0;
						    this.play();
						}, false);

		            	App.StartScreen.sounds.alarm1.play();
		            	App.StartScreen.sounds.alarm2.play();

		            	$overlay.addClass('animated');
		            	$jail.addClass('animated');

		            	setTimeout(function(){

		            		$terminal.addClass('animated')
		            			.find('span')
		            			.typed({
		            				strings:[
		            				"Beste Jeroen, hier spreekt hacker Piet...",
									"En zo makkelijk krijg jij dit jaar<br>jouw kadootjes niet...",
									"Volg deze instructies en luister <br> naar wat ik zeg...",
									"Anders zijn jouw kadootjes... WEG! </br> ^1200 Oja, de tijd loopt..."
		            				],
		            				startDelay: 2000,
		            				callback: function() {
		            					addCountdown();
		            					$('.typed-cursor').css('opacity', 0);
		            				}
		            			});


		            	}, 7000);
		            }
				});
			}, 3000);

		}

		function addSounds() {
			
			App.StartScreen.sounds.klaas.play();
		}

		function addCountdown() {

		    var count = 500;   
		    var counter = setInterval(timer, 50); 
		    
		    function timer()
		    {
		        if (count <= 0)
		        {
		            clearInterval(counter);
		            return;
		         }
		         count--;
		         document.getElementById("timer").innerHTML=count /50; 
		    }

		    timer();

		    setTimeout(function(){
		    	$('#next-button').css({
		    		display: 'block'
		    	});
		    }, 5000);

		}


        /* --------------------------------------- */

        return obj;


    })();


/* ----------------------------------------------- */
