/**
*   Concrete 5 Default
*   Gulp
*
*   @copyright (c) 2015 Acato B.V.
*   @author: jorik@acato.nl
*   @version 0.0.1
*
*   @ Gulpfile (main)
*/

/* ----------------------------------------------- */

		var gulp 		= require('gulp'),
	        moment      = require('moment'),
	        browserSync = require('browser-sync'),
	        reload      = browserSync.reload;

	/* ------------------------------------------- */
	/**
	* 	Gulp -> Config
	*
	*/

		var project 	= 'Surprise';

		var src 		= '../',
			dist 		= '../../dist/';

		var src_css 	= src + 'styles/',
			src_js 		= src + 'scripts/',
			src_assets 	= src + 'assets/',
			src_bower 	= src + 'bower_components/';

		var dist_css 	= dist + 'css/',
			dist_js  	= dist + 'js/',
			dist_assets = dist + 'assets/';


	/* ------------------------------------------- */
	/**
	* 	Gulp -> onError
	*	Error Handling (plumber)
	*
	*/
	
		var onError = function(error) {
	        plugins.util.beep();

	        plugins.util.log(
	            plugins.util.colors.red(
	                '[' + project + '] Error (' + error.plugin + '): ' + error.message
	            )
	        );

	        this.emit('end');
		};
	
	
	/* ------------------------------------------- */
	/**
	* 	Gulp -> Notify Handler
	*
	*/

		var showNotification = function(mess) {
			return {
				title: project.toUpperCase(),
				onLast: true,
				message:
					mess + '\n' +
					moment().format('dddd DD MMMM, HH:mm:ss')
			};
		};


	/* ------------------------------------------- */
	/**
	* 	Gulp -> Load Plugins
	*
	*	Loads all Gulp Plugins
	*	syntax below is the same (but in short) as:
	*	var gulpLoadPlugins = require('gulp-load-plugins');
	*	var plugins 		= gulpLoadPlugins();
	*
	*/
	
		var plugins = require('gulp-load-plugins')(),
			moment 	= require('moment'),
			size 	= require('gulp-filesize');
	

	/* ------------------------------------------- */
	/**
	* 	Gulp -> Task 'assets'
	*
	*/

		gulp.task('images', function() {
			return gulp.src(src_assets + 'images/*')
				.pipe(plugins.plumber({errorHandler: onError}))
				.pipe(plugins.cache(plugins.imagemin({
					optimizationLevel: 9,
					progressive: true,
					interlaced: true 
				})))
				.pipe(gulp.dest(dist_assets + 'images'))
				.pipe(size())
		        .pipe(plugins.notify(showNotification('Images Optimized')));
		});


	/* ------------------------------------------- */
	/**
	* 	Gulp -> Task 'less'
	*
	*/

		gulp.task('less', function() {
			return gulp.src( src_css + 'main.less' )
				.pipe(plugins.plumber({errorHandler: onError}))
				.pipe(plugins.less())
				.pipe(plugins.concat(project + '.css'))
				.pipe(gulp.dest(dist_css))
				.pipe(plugins.minifyCss({
					keepBreaks: true
				}))
				.pipe(plugins.rename(project + '.min.css'))
				.pipe(gulp.dest(dist_css))
				.pipe(size())
		        .pipe(plugins.notify(showNotification('Styles Optimized')));
		});


	/* ------------------------------------------- */
	/**
	* 	Gulp -> Task 'jshint'
	* 	covers everything under src/*.js
	*	ignores:
	*		'_src/js/vendor/'
	*		'_src/js/plugins/'
	*		'_src/grunt/'
	*		'_src/gulp/'
	*
	*/

		gulp.task('jshint', function() {
			return gulp.src([
					src_js + '**/*.js',
					'!' + src_js + 'vendor/**/*.js',
					'!' + src_js + 'plugins/**/*.js',
					'!' + src + 'gulp/**/*.js',
					'!' + src + 'grunt/**/*.js'
				])
				.pipe(plugins.plumber({errorHandler: onError}))
				.pipe(plugins.jshint())
				.pipe(plugins.jshint.reporter('default'))
		        .pipe(plugins.notify(showNotification('Scripts Checked (jsHint)')));
		});


	/* ------------------------------------------- */
	/**
	* 	Gulp -> Task 'scripts'
	*
	*/

		gulp.task('scripts', function() {
			return gulp.src([
					src_js + 'vendors/jquery.lib.min.js',
					src_js + 'vendors/modernizr.js',
					src_js + 'vendors/sha512.js',
					src_bower + 'typed.js/js/typed.js',
					src_js + 'plugins/**/*.js',
					src_js + 'app.fx.js',
					src_js + 'helpers/**/*.js',
					src_js + 'utilities/**/*.js',
					src_js + 'modules/**/*.js',
					src_js + 'app.js'
				])
				.pipe(plugins.plumber({errorHandler: onError}))
				.pipe(plugins.concat(project + '.js'))
				.pipe(gulp.dest(dist_js))
				// .pipe(plugins.uglify())
				.pipe(plugins.rename(project + '.min.js'))
				.pipe(gulp.dest(dist_js))
				.pipe(size())
		        .pipe(plugins.notify(showNotification('Scripts Optimized')));
		});

     /* ------------------------------------------- */
     /**
     *   Gulp -> Task 'bs-reload'
     *   Trigger the Reload in Browsersync
     *
     */
 
     gulp.task('bs-reload', function () {
         browserSync.reload();
     });
 
 
     /* ------------------------------------------- */
     /**
     *   Gulp -> Task 'browser-sync'
     *   Prepare Browser-Sync for localhost
     *
     */
 
     gulp.task('browser-sync', function() {
         browserSync.init({
            proxy: 'localhost/'
         });
     });
 
 
     /* ------------------------------------------- */
     /**
     *   Gulp -> Task 'default'
     *   Run the default tasks & start the watchers
     *
     */
 
     gulp.task('watch', ['less', 'scripts', 'browser-sync'], function () {
 			gulp.watch(src_js 	+ '**/*.js', 	['scripts']);
			gulp.watch(src_css 	+ '**/*.less', 	['less']);

			gulp.watch('../../**/*.html', 		['bs-reload']);

			// Work Around for injected css
			gulp.watch(dist_css + '**/*.css').on('change', function() {
				browserSync.reload('*.css');
			});

			// Work Around for injected JS
			gulp.watch(dist_js + '**/*.js').on('change', function() {
				browserSync.reload('*.js');
			});
		 });


	/* ------------------------------------------- */
	/**
	* 	Gulp -> Task 'default'
	*
	*/

		gulp.task('default', [
			'watch'
		]);


/* ----------------------------------------------- */
